import React, { Component } from 'react';
import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="search-box">
          <input type="text" placeholder="Search..." className="search-input" spellcheck="false"/>
          <a className="search-btn" href="#">
            <FontAwesomeIcon icon={faSearch} />
          </a> 
        </div>
        <div className="player-box">
          <div className="picture-box">
            <img src="https://i0.wp.com/istbasket.com.tr/wp-content/uploads/2016/11/ahmet-d%C3%BCverio%C4%9Fl%C4%B1.png?fit=630%2C630"/>
            <div className="picture-settings">
              <div className="picture-edit" >
                <FontAwesomeIcon className="picture-edit-icon" icon={faEdit} />
                <div className="picture-edit-text">EDIT</div>
              </div>
              <div className="picture-delete" href="#">
                <FontAwesomeIcon className="picture-delete-icon" icon={faTrashAlt} />
                <div className="picture-delete-text">DELETE</div>
              </div>
            </div>
          </div>
          <div className="features">
            <div className="features-upper">
              <div className="features-box">
                <div>POINTS</div>
                <div className="features-box-line"></div>
                <div>19.3</div>
              </div>
              <div className="features-box">
                <div>REBOUNDS</div>
                <div className="features-box-line"></div>
                <div>11.0</div>
              </div>
              <div className="features-box">
                <div>ASSISTS</div>
                <div className="features-box-line"></div>
                <div>2.6</div>
              </div>
            </div>
            <div className="features-lower">
              <div className="features-box">
                <div>CONTRACT END</div>
                <div className="features-box-line"></div>
                <div>01/07/2019</div>
              </div>
              <div className="features-box">
                <div>HAPPINESS</div>
                <div className="features-box-line"></div>
                <div>GOOD</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
